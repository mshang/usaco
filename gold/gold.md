| Contest  | Name         | Status         |
| :------- | :----------- | :------------- |
| 2016 Dec | moocast      | solved 1/29/19 |
| 2017 Dec | barnpainting | solved 2/10/19 |
| 2019 Jan | poetry       | solved 2/5/19  |
|          | sleepy       | solved 2/5/19  |
|          | shortcut     | solved 2/5/19  |