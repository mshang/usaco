| Contest  | Name        | Status          |
| :------- | :---------- | :-------------- |
| 2015 Dec | lightson    | solved 2/1/19   |
|          | highcard    | solved 1/31/19  |
|          | bcount      | solved 1/30/19  |
| 2016 Dec | haybales    | solved 1/28/19  |
|          | citystate   | solved 1/29/19  |
|          | moocast     | solved 1/28/19  |
| 2017 Dec | homework    | solved 1/08/19  |
|          | measurement |                 |
|          | shuffle     |                 |
| 2018 Jan | lifeguards  |                 |
|          | rental      |                 |
| 2018 Feb | reststops   | solved 1/26/19  |
|          | snowboots   |                 |
| 2018 Dec | convention  |                 |
|          | convention2 | solved 12/24/18 |
|          | mooyomooyo  | solved 12/19/18 |
| 2019 Jan | planting    | solved 1/21/19  |
|          | perimeter   | solved 1/21/19  |
|          | mountains   | solved 1/21/19  |